<h1 align="center">
  <br>
  <a href="https://www.morphysm.com/"><img src="./assets/morph_logo_rgb.png" alt="Morphysm" ></a>
  <br>
  <h5 align="center"> Morphysm is a community of engineers, designers and researchers
contributing to security, cryptography, cryptocurrency and AI.</h5>
  <br>
</h1>

<h1 align="center">
  <img src="https://img.shields.io/badge/Python-3.9-red" alt="python badge">

 <img src="https://img.shields.io/badge/docker-20.10-blue" alt="docker badge">
 <img src="https://img.shields.io/badge/version-2.8.2-orange" alt="version badge">
 <img src="https://img.shields.io/gitlab/pipeline-status/dicu.chat/server?branch=master" alt="docker build">
</h1>

# Table of Contents

<!--ts-->

- [Rasa](#rasa)
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Demo](#demo)
- [Testing our Model](#testing-our-model)
- [Troubleshooting](#troubleshooting)
- [Code Owners](#code-owners)
- [License](#license)
- [Contact](#contact)
<!--te-->

# Rasa

This repository has a collection of containerized AI models, with GPU support and simple one-endpoint API.

Rasa is a natural language processing (NLP) library for our server that allows us to do many small NLP tasks on the backend. Some tasks we use rasa for are: extracting language from user input, mapping user's country/flag emoji to language(s), extracting numbers/dates from natural language input.

The dicu.chat rasa container is housed in this repository. The core server talks with the rasa container in order to construct a chat flow between the bot and the user, the goal of this repository is primarily to detect the user's language and conduct conversations that are translated based on the user's language.

# Prerequisites

Please make sure that your system has the following programs:

- `python 3.9`
- `docker`
- `docker-compose`

# Installation

1. First clone the repo either through SSH:

   ```bash
   git clone git@gitlab.com:dicu.chat/rasa.git
   ```

   or through HTTPS:

   ```bash
   git clone https://gitlab.com/dicu.chat/rasa.git
   ```

2. Install environment:

   ```bash
    virtualenv -p python3.9 .venv
    source .venv/bin/activate
   ```

3. Install dependencies:

   ```bash
   python -m pip install -U pip wheel setuptools && python -m pip install -r requirements.txt
   ```

4. Create the configration file for docker:

   ```
   vim docker-compose.yml
   ```

5.  Run the project using docker

   ```
   docker-compose up
   ```

6. For testing and to check if rasa is working go to `0.0.0.0:5005`

# Demo:

Will be updated soon..

# Testing our Model:

To put our model to the test and start a conversation with a rasa model's bot, Please run the [server repo](https://gitlab.com/dicu.chat/server) with this repo so they can start talking to each other. You must also run the [bridge telegram repo](https://gitlab.com/dicu.chat/bridge-telegram), but change the RASA_URL in the server repo's [.env file on line 8](https://gitlab.com/dicu.chat/server/-/blob/master/.env.example#L8) to the default port 5005. Then, go to your Telegram bot channel and type /start then /ai to begin a conversation.

# Troubleshooting

If you have encountered any problems while running the code, please open a new issue in this repo and label it bug, and we will assist you in resolving it.

# Code Owners

@morphysm/team :sunglasses:

# License:

Our repository is licensed under the terms of the GNU Affero General Public License v3.0 see the license [here!](https://gitlab.com/dicu.chat/rasa/-/blob/master/LICENSE)

# Contact

If you'd like to know more about us [Click here!](https://www.morphysm.com/), or you can contact us at "contact@morphysm.com".
